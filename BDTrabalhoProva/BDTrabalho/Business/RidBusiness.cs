﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Business
{
    class RidBusiness
    {
        Database.RidDatabase riddatabase = new Database.RidDatabase();
        public void NovaSaida(Model.RidModel model)
        {
            if (model.Piloto == string.Empty)
            {
                throw new ArgumentException("Piloto obrigatorio");
            }

            if (model.Carro == string.Empty)
            {
                throw new ArgumentException("Carro obrigatorio");
            }

            if (model.Saida == null)
            {
                throw new ArgumentException("Saída obrigatoria");
            }

            riddatabase.NovaSaida(model);
        }

        public List<Model.RidModel> Fitrar(string piloto, DateTime dia)
        {
            List<Model.RidModel> lista = riddatabase.Fitrar(piloto, dia);

            return lista;
        }

        public Model.RidModel FiltrarPorID(int id)
        {
            Model.RidModel model = riddatabase.FiltrarPorId(id);

            return model;
        }

        public void MarcarChegda (Model.RidModel model)
        {
            if (model.Chegada == null)
            {
                throw new ArgumentException("Chegada obrigatorio");
            }

            riddatabase.MarcarChegada(model);
        }

        public void AtribuirMulta (Model.RidModel model)
        {
            if (model.MultaPreco == 0)
            {
                throw new ArgumentException("Valor é obrigatorio");
            }

            riddatabase.AtribuirMulta(model);
        }

        public void RemoverSaida (int id)
        {
            riddatabase.RemoverSaida(id);
        }
    }
}

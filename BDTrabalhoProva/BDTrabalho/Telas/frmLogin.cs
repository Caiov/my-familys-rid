﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtLogin.Text;
                string senha = txtSenha.Text;

                if (usuario != "Caio")
                {
                    MessageBox.Show("Usuario incorreto");
                }
                else if (usuario == "Caio" && senha != "28626996")
                {
                    MessageBox.Show("Senha incorreta");
                }
                else
                {
                    frmMenu tela = new frmMenu();
                    tela.Show();
                    this.Close();
                }
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

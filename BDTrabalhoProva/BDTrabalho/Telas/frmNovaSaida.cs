﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho
{
    public partial class frmNovaSaida : Form
    {
        public frmNovaSaida()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Model.RidModel model = new Model.RidModel();
                TimeSpan saida = TimeSpan.Parse(txtSaida.Text);
                model.Piloto = txtPiloto.Text;
                model.Carro = txtCarro.Text;
                model.Dia = dtpDia.Value;
                model.Saida = saida;

                Business.RidBusiness ridbusinees = new Business.RidBusiness();
                ridbusinees.NovaSaida(model);

                MessageBox.Show("Saida marcada com sucesso");
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
            
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho
{
    public partial class frmAtribuirMulta : Form
    {
        public frmAtribuirMulta()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Model.RidModel model = new Model.RidModel();
                model.Id = Convert.ToInt32(txtId.Text);
                model.MultaPreco = nudValor.Value;
                model.MultaPontos = Convert.ToInt32(cboPontos.Text);
                model.TeveMulta = true;

                Business.RidBusiness ridbusiness = new Business.RidBusiness();
                ridbusiness.AtribuirMulta(model);

                MessageBox.Show("Multa atribuida com sucesso");
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro, tente novamente");
            }
           
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho
{
    public partial class frmRemoverSaida : Form
    {
        public frmRemoverSaida()
        {
            InitializeComponent();
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtId.Text);

                Business.RidBusiness ridbusiness = new Business.RidBusiness();
                Model.RidModel model = ridbusiness.FiltrarPorID(id);

                if (model != null)
                {
                    txtPiloto.Text = model.Piloto;
                    dtpDia.Value = model.Dia;
                    panelConfirmacao.Visible = true;
                }
                else
                {
                    txtPiloto.Text = string.Empty;
                    dtpDia.Value = DateTime.Now;
                }
            }
            catch
            {
                MessageBox.Show("Campo ID deve ser preenchido");
            }
        }

        private void btnSim_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtId.Text);

                Business.RidBusiness ridbusiness = new Business.RidBusiness();
                ridbusiness.RemoverSaida(id);

                MessageBox.Show("Deletado com sucesso");
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }


        private void btnNao_Click(object sender, EventArgs e)
        {
            try
            {
                txtId.Text = string.Empty;
                txtPiloto.Text = string.Empty;
                dtpDia.Value = DateTime.Now;
                panelConfirmacao.Visible = false;
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho
{
    public partial class frmMarcarChegada : Form
    {
        public frmMarcarChegada()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Model.RidModel model = new Model.RidModel();
                TimeSpan chegada = TimeSpan.Parse(txtChegada.Text);
                model.Id = Convert.ToInt32(txtId.Text);
                model.Chegada = chegada;
                model.Observacao = txtObs.Text;

                Business.RidBusiness ridbusiness = new Business.RidBusiness();
                ridbusiness.MarcarChegda(model);

                MessageBox.Show("Chegada marcada com sucesso");
            }
            catch
            {
                MessageBox.Show("Ocorreu um erro");
            }
            
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
